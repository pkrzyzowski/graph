﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

using Graph.DTO;
using Graph.Api.Repository;
using System.Web.Http.Cors;

namespace Graph.Api.Controllers
{
    public class PathController : ApiController
    {
        private IRepository repository;
        
        public PathController(IRepository repository)
        {
            this.repository = repository;
        }   
        
        [HttpGet]
        public async Task<IHttpActionResult> Get(string startingPoint, string destination)
        {
            Node startingNode = await this.repository.GetNode(startingPoint);
            var destinationNode = await this.repository.GetNode(destination);

            if (startingNode == null || destinationNode == null) return BadRequest();
            
            Dictionary<string, string> visitedNodes = new Dictionary<string, string>();
            List<string[]> queue = new List<string[]>() { new string[] { startingPoint, string.Empty } };

            while(queue.Any())
            {
                string[] currentNode = queue.First();
                queue.RemoveAt(0);

                if (currentNode[0] == destination)
                {
                    visitedNodes.Add(currentNode[0], currentNode[1]);
                    break;
                }

                if (visitedNodes.ContainsKey(currentNode[0])) continue;
                Node currentNodeObject = await this.repository.GetNode(currentNode[0]);
                if(currentNodeObject == null)
                {
                    continue;
                }

                foreach(string unvisitedAdjacent in currentNodeObject.adjacentNodes.Where(p => !visitedNodes.ContainsKey(p)).ToList())
                {
                    queue.Add(new string[] { unvisitedAdjacent, currentNode[0] });
                }
                visitedNodes.Add(currentNode[0], currentNode[1]);
            }
            if (visitedNodes.Last().Key != destination) return NotFound();
            List<string> shortestPath = GetPreviousNode(visitedNodes, visitedNodes.Last().Key);

            return Ok(shortestPath);
        }

        private List<string> GetPreviousNode(Dictionary<string,string> visitedNodes, string previousNode)
        {
            List<string> path = new List<string>();
            if (visitedNodes.ContainsKey(previousNode))
            {
                path.Add(previousNode);
                path.AddRange(GetPreviousNode(visitedNodes, visitedNodes[previousNode]));
            }
            return path;
        }
    }
}
