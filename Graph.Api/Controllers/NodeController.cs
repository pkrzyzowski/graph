﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Graph.Api.Repository;
using Graph.DTO;
using Graph.Api.Headers;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using System.Web.Http.Cors;

namespace Graph.Api.Controllers
{ 
    public class NodeController : ApiController
    {
        private IRepository repository;

        public NodeController(IRepository repository)
        {
            this.repository = repository;
        }

        private async Task<bool> EnsureAdjacentNodeHasReferenceToCurrentNode(string nodeId, string adjacentNodeId)
        {
            Node adjacentNode = await this.repository.GetNode(adjacentNodeId);
            if (adjacentNode == null) return true;

            bool success = true;

            if (!adjacentNode.adjacentNodes.Contains(nodeId))
            {
                success = success && await this.repository.AddAdjacent(adjacentNodeId, nodeId);
            }

            return success;
        }

        private async Task AddRelationsToAdjacentNodes(Node node)
        {
            foreach (var adjacentId in node.adjacentNodes)
            {
                await EnsureAdjacentNodeHasReferenceToCurrentNode(node.id, adjacentId);
            }
        }

        private async Task<Node> AddMissingRelationsToNode(Node node)
        {
            IEnumerable<string> unknownAdjacents = await this.repository.FindAdjacents(node.id);
            node.adjacentNodes.AddRange(unknownAdjacents.Where(p=>!node.adjacentNodes.Contains(p)));
            return node;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]Node node)
        {
            try
            {
                node = await AddMissingRelationsToNode(node);

                Node insertedNode = await this.repository.InsertNode(node);
                await AddRelationsToAdjacentNodes(node);

                return Ok(insertedNode);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            try
            {
                if (!await this.repository.NodeExists(id))
                {
                    return NotFound();
                }

                long deletedCount = await this.repository.RemoveNode(id);
                if (deletedCount > 0) return StatusCode(HttpStatusCode.NoContent);
                else return StatusCode(HttpStatusCode.NotModified);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put([FromBody]Node node)
        {
            try
            {
                if(!await this.repository.NodeExists(node.id))
                {
                    return NotFound();
                }

                node = await AddMissingRelationsToNode(node);

                Node result = await this.repository.UpdateNode(node);
                if (result != null)
                {
                    await AddRelationsToAdjacentNodes(node);
                    return Ok(result);
                }
                else return StatusCode(HttpStatusCode.NotModified);
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
        
        [HttpGet]
        public async Task<IHttpActionResult> Get(string id)
        {
            try
            {
                Node result = await this.repository.GetNode(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int pageNumber = 1, int? pageSize = null)
        {
            try
            {
                long nodesCount = await this.repository.CountNodes();
                if (nodesCount == 0) return StatusCode(HttpStatusCode.NoContent);

                if (pageSize == null)
                {
                    return Ok(await this.repository.GetNodes());
                }

                UrlHelper urlHelper = new UrlHelper(Request);
                int pagesCount = (int)Math.Ceiling((float)nodesCount / (int)pageSize);

                string paginationHeader = Newtonsoft.Json.JsonConvert.SerializeObject(
                    new PaginationHeader
                    {
                        CurrentPage = pageNumber,
                        PageSize = (int)pageSize,
                        PagesCount = pagesCount,
                        PreviousPage = pageNumber > 1 ? urlHelper.Link(Constans.NodesControllerGetWithPaginationRoute, new { pageNumber = pageNumber - 1, pageSize = pageSize }) : null,
                        NextPage = pageNumber < pagesCount ? urlHelper.Link(Constans.NodesControllerGetWithPaginationRoute, new { pageNumber = pageNumber + 1, pageSize = pageSize }) : null
                    }
                );
                
                Request.GetOwinContext().Response.Headers.Append(Headers.PaginationHeader.PaginationHeaderName, paginationHeader);

                int? skip = (pageNumber - 1) * pageSize;

                return Ok(
                    await this.repository.GetNodes((pageNumber-1) * pageSize, pageSize)
                );
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
