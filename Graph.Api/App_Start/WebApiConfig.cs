﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Graph.Api.App_Start
{
    public static class WebApiConfig
    {
        public static HttpConfiguration RegisterRoutes(HttpConfiguration config)
        {
            config.EnableCors(
                new EnableCorsAttribute(
                    origins: "*",
                    headers: "*",
                    methods: "*"
                )
            );
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: Constans.PathControllerGetRoute,
                routeTemplate: Constans.WebApiRoutePrefix + "/path/{startingPoint}/{destination}",
                defaults: new { controller = "path" }
            );

            config.Routes.MapHttpRoute(
                name: Constans.DefaultApiRoute,
                routeTemplate: Constans.WebApiRoutePrefix + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: Constans.NodesControllerGetWithPaginationRoute,
                routeTemplate: Constans.WebApiRoutePrefix + "/nodes/{pageNumber}/{pageSize}",
                defaults: new { controller = "node", id = RouteParameter.Optional, pageNumber = RouteParameter.Optional, pageSize = RouteParameter.Optional }
            );


            return config;
        }
    }
}