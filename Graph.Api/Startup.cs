﻿using System.Web.Http;
using Owin;
using Microsoft.Owin;
using Autofac;
using System.Reflection;
using Autofac.Integration.WebApi;
using Graph.Api.Repository;

[assembly:OwinStartup(typeof(Graph.Api.Startup))]
namespace Graph.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config = App_Start.WebApiConfig.RegisterRoutes(config);

            var builder = new ContainerBuilder();

            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterApiControllers(currentAssembly).InstancePerRequest();
            builder.RegisterType<MongoDbRepository>().As<IRepository>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }
    }
}