﻿namespace Graph.Api.UnitTests

open Ploeh.AutoFixture
open NUnit.Framework
open FsUnit
open Foq

open Graph.DTO
open Graph.Api
open Graph.Api.Controllers
open Graph.Api.Repository
open Graph.Api.Headers

open System
open System.Threading.Tasks
open System.Web
open System.Net
open System.Net.Http
open System.Web.Http
open System.Web.Http.Results
open System.Web.Http.Routing
open System.Collections.Generic
open System.Linq

open Microsoft.Owin

[<TestFixture>]
type NodeControllerTests() = 

    
    let mutable controller:NodeController = null
    let mutable fixture:Fixture = null
    let mutable owinContext = null
    let baseUri = new Uri("http://localhost")

    let getController(repository) = 
        let configuration = App_Start.WebApiConfig.RegisterRoutes(new HttpConfiguration())

        controller <- new NodeController(repository)
        let mockedRequest = new HttpRequestMessage()
        mockedRequest.RequestUri <- baseUri

        controller.Request <- mockedRequest
        controller.Configuration <- configuration
        controller.Request.SetOwinContext(owinContext)
        
    
    let getNewNodeForPostAndPut() = new Node( id = "3", label="Sample3", adjacentNodes = new List<string>(["1"]))

    let mockRepositoryForPostPut(newNodeExists:bool) = 
        let nodesInDatabase = [|
            new Node( id = "1", label = "Sample1", adjacentNodes = new List<string>([ "2" ]) )
            new Node( id = "2", label = "Sample2", adjacentNodes = new List<string>([ "3" ]) )
        |]
        let expectedNode = new Node( id = "3", label = "Sample3", adjacentNodes = new List<string>(["1";"2"]))

        Mock<IRepository>()
            .Setup(fun x -> <@ x.GetNode("1") @>).Returns(Task.FromResult(nodesInDatabase.Single(fun x -> x.id = "1")))
            .Setup(fun x -> <@ x.GetNode("2") @>).Returns(Task.FromResult(nodesInDatabase.Single(fun x -> x.id = "2")))
            .Setup(fun x -> <@ x.FindAdjacents("3") @>).Returns(Task.FromResult(nodesInDatabase.Where(fun x -> x.adjacentNodes.Contains("3")).Select(fun x -> x.id).AsEnumerable()))
            .Setup(fun x -> <@ x.InsertNode(any()) @>).Returns(Task.FromResult(expectedNode))
            .Setup(fun x -> <@ x.AddAdjacent("1", "3") @>).Returns(Task.FromResult(true))
            .Setup(fun x -> <@ x.NodeExists("3") @>).Returns(Task.FromResult(newNodeExists))
            .Setup(fun x -> <@ x.UpdateNode(any()) @>).Returns(Task.FromResult(expectedNode))
            .Create()

    let verifyPostPut(result:IHttpActionResult, mockedRepository:IRepository) = 
        result |> should be instanceOfType<OkNegotiatedContentResult<Node>>
        verify <@ mockedRepository.AddAdjacent("1", "3") @> once
        (result :?> OkNegotiatedContentResult<Node>).Content.adjacentNodes |> should contain "2"

    let executeDeletion(numberOfAffectedRows:int64) = 
        let mockedRepository = Mock<IRepository>()
                                .Setup(fun x -> <@ x.NodeExists(any()) @>).Returns(Task.FromResult(true))
                                .Setup(fun x -> <@ x.RemoveNode(any()) @>).Returns(Task.FromResult(numberOfAffectedRows))
                                .Create()
        getController(mockedRepository)
        controller.Delete("").Result

    let getActualPaginationHeader() = 
        Newtonsoft.Json.JsonConvert.DeserializeObject<PaginationHeader>(
            owinContext.Response.Context.Response.Headers.Get(PaginationHeader.PaginationHeaderName)
        )

    let getListOfNodes(numberOfNodes:int) =
        fixture.CreateMany<Node>(numberOfNodes)
    
    let prepareRepositoryForPaginationTests(skip:int, take:int, result) = 
        Mock<IRepository>()
            .Setup(fun x -> <@ x.CountNodes() @>).Returns(Task.FromResult(3L))
            .Setup(fun x -> <@ x.GetNodes(is(fun v -> v.Value = skip), is(fun v -> v.Value = take)) @>).Returns(Task.FromResult(result))
            .Create()
    
    let AssertPagination(result:IHttpActionResult, pageSize:int, currentPage:int, previousPage:bool, nextPage:bool, pagesCount:int) =
        result |> should be instanceOfType<OkNegotiatedContentResult<IEnumerable<Node>>>
        let contentResult = (result :?> OkNegotiatedContentResult<IEnumerable<Node>>)
        let paginationHeader = getActualPaginationHeader()
        contentResult.Content.Count() |> should equal pageSize
        paginationHeader.CurrentPage |> should equal currentPage
        paginationHeader.PreviousPage |> if previousPage then (should not' (be null)) else (should be null)
        paginationHeader.NextPage |> if nextPage then (should not' (be null)) else (should be null)
        paginationHeader.PageSize |> should equal 1
        paginationHeader.PagesCount |> should equal 3

    [<SetUp>]
    member this.Setup() = 
        fixture <- new Fixture()
        owinContext <- new OwinContext()

    [<TearDown>]
    member this.TearDown() = 
        if controller <> null then controller.Dispose()

    [<Test>]
    member this.``Node controller - Get method should return first page of nodes, and response headers should contain PaginationHeader.``() = 
        //Arrange:
        let nodes = getListOfNodes(1)
        let mockedRepository = prepareRepositoryForPaginationTests(0,1,nodes)
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Get(1, Nullable(1)).Result

        //Assert:
        AssertPagination(result, 1, 1, false, true, 3)


    [<Test>]
    member this.``Node controller - Get method should return the page of nodes, and response headers should contain PaginationHeader.``() =
        //Arrange:
        let nodes = getListOfNodes(1)
        let mockedRepository = prepareRepositoryForPaginationTests(1,1,nodes)
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Get(2,Nullable(1)).Result

        //Assert:
        AssertPagination(result, 1, 2, true, true, 3)

    [<Test>]
    member this.``Node controller - Get method should return last page of nodes, and response headers should contain PaginationHeader.``() =
        //Arrange:
        let nodes = getListOfNodes(1)
        let mockedRepository = prepareRepositoryForPaginationTests(2,1,nodes)
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Get(3,Nullable(1)).Result

        //Assert:
        AssertPagination(result, 1, 3, true, false, 3)

    [<Test>]
    member this.``Node controller - Get method should return complete list of nodes when page size is null``() = 
       //Arrange:
       let numberOfNodes = 20
       let nodes = fixture.CreateMany<Node>(numberOfNodes)
       let mockedRepository = Mock<IRepository>()
                                .Setup(fun x -> <@ x.CountNodes() @>).Returns(Task.FromResult(int64(numberOfNodes)))
                                .Setup(fun x -> <@ x.GetNodes(any(), any()) @>).Returns(Task.FromResult(nodes))
                                .Create()
       
       getController(mockedRepository) |> ignore

       //Act:
       let result = controller.Get(1, new Nullable<_>()).Result

       //Assert:
       result |> should be instanceOfType<OkNegotiatedContentResult<IEnumerable<Node>>>
       (result :?> OkNegotiatedContentResult<IEnumerable<Node>>).Content.Count() |> should equal numberOfNodes

    [<Test>]
    member this.``Node controller - Get method should return 404 status code when nodeId is not provided.``() = 
        //Arrange:
        let mockedRepository = Mock<IRepository>.With(fun mock -> <@ mock.GetNode(any()) --> Task.FromResult(null) @>)
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Get(null).Result
        
        //Assert:
        result |> should be instanceOfType<NotFoundResult>

    [<Test>]
    member this.``Node controller - Get method should return a single Node.``() =
        //Arrange:
        let testNode = fixture.Create<Node>()
        let mockedRepository = Mock<IRepository>.With(fun mock -> <@ mock.GetNode(any()) --> Task.FromResult(testNode) @>)
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Get(null).Result
        
        //Assert:
        result |> should be instanceOfType<OkNegotiatedContentResult<Node>>
        (result :?> OkNegotiatedContentResult<Node>).Content |> should equal testNode

    [<Test>]
    member this.``Node controller - Post method should insert node and update adjacents.``() = 
        //Arrange:
        let mockedRepository = mockRepositoryForPostPut(false)
        let newNode = getNewNodeForPostAndPut()
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Post(newNode).Result;

        //Assert:
        verifyPostPut(result, mockedRepository)

    [<Test>]
    member this.``Node controller - Put method should update node and update adjacents``() =
        //Arrange:
        let mockedRepository = mockRepositoryForPostPut(true)
        let newNode = getNewNodeForPostAndPut()
        getController(mockedRepository) |> ignore

        //Act:
        let result = controller.Put(newNode).Result

        //Assert:
        verifyPostPut(result, mockedRepository)

    [<Test>]
    member this.``Node controller - Put method should return 404 if node cannot be found..``() =
        //Arrange:
        let mockedRepository = Mock<IRepository>.With(fun mock -> <@ mock.NodeExists(any()) --> Task.FromResult(false) @>)
        getController(mockedRepository)

        //Act:
        let result = controller.Put(getNewNodeForPostAndPut()).Result

        //Assert:
        result |> should be instanceOfType<NotFoundResult>

    [<Test>]
    member this.``Node controller - Put method should return not modified if update was unsuccessful.``() =
        //Arrange:
        let mockedRepository = Mock<IRepository>()
                                .Setup(fun x -> <@ x.GetNode(any()) @>).Returns(Task.FromResult(new Node()))
                                .Setup(fun x -> <@ x.FindAdjacents(any()) @>).Returns(Task.FromResult(seq { yield "" }))
                                .Setup(fun x -> <@ x.AddAdjacent(any(), any()) @>).Returns(Task.FromResult(true))
                                .Setup(fun x -> <@ x.NodeExists(any()) @>).Returns(Task.FromResult(true))
                                .Setup(fun x -> <@ x.UpdateNode(any()) @>).Returns(Task.FromResult(null))
                                .Create()

        getController(mockedRepository)
        
        //Act:
        let result = controller.Put(getNewNodeForPostAndPut()).Result

        //Assert:
        result |> should be instanceOfType<StatusCodeResult>
        (result :?> StatusCodeResult).StatusCode |> should equal HttpStatusCode.NotModified

    [<Test>]
    member this.``Node controller - Delete method should return not found on attempt of removing unexisting node.``() = 
        //Arrange:
        let mockedRepository = Mock<IRepository>.With(fun mock -> <@ mock.NodeExists(any()) --> Task.FromResult(false) @>)
        getController(mockedRepository)

        //Act:
        let result = controller.Delete("").Result;

        //Assert:
        result |> should be instanceOfType<NotFoundResult>

    [<Test>]
    member this.``Node controller - Delete method should return no content after successful deletion.``() =
        //Arrange:
        //Assert:
        let result = executeDeletion(1L)

        //Assert:
        result |> should be instanceOfType<StatusCodeResult>
        (result :?> StatusCodeResult).StatusCode |> should equal HttpStatusCode.NoContent

    [<Test>]
    member this.``Node controller - Delete method should return not modified after unsuccessful deletion.``() =
        //Arrange:
        //Assert:
        let result = executeDeletion(0L)

        //Assert:
        result |> should be instanceOfType<StatusCodeResult>
        (result :?> StatusCodeResult).StatusCode |> should equal HttpStatusCode.NotModified
