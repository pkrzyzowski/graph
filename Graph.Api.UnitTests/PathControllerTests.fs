﻿namespace Graph.Api.UnitTests

open Ploeh.AutoFixture
open NUnit.Framework
open FsUnit
open Foq

open Graph.DTO
open Graph.Api
open Graph.Api.Controllers
open Graph.Api.Repository
open Graph.Api.Headers

open System
open System.Threading.Tasks
open System.Web
open System.Net
open System.Net.Http
open System.Web.Http
open System.Web.Http.Results
open System.Web.Http.Routing
open System.Collections.Generic
open System.Linq

open Microsoft.Owin

[<TestFixture>]
type PathControllerTests() = 
    
    let mutable controller = null
    let mutable fixture = new Fixture()

    let getController(repository) = 
        controller <- new PathController(repository)

    let setupNodeEntity(repository:Mock<IRepository>, node:Node) = 
        repository.Setup(fun x -> <@ x.GetNode(node.id) @>).Returns(Task.FromResult(node))

    [<SetUp>]
    member this.Setup() = 
        fixture <- new Fixture()

    [<TearDown>]
    member this.TearDown() = 
        if controller <> null then controller.Dispose()

    [<Test>]
    member this.``Path controller - Get method should return bad request status if starting node doesn't exist``() =
        //Arrange:
        let repository = Mock<IRepository>()
                            .Setup(fun x -> <@ x.GetNode("1") @>).Returns(Task.FromResult(null))
                            .Setup(fun x -> <@ x.GetNode("2") @>).Returns(Task.FromResult(null))
                            .Create()

        getController(repository)

        //Act:
        let result = controller.Get("1", "2").Result

        //Assert:
        result |> should be instanceOfType<BadRequestResult>

    [<Test>]
    member this.``Path controller - Get method should return bad request status if destination node doesn't exist``() =
        //Arrange:
        let repository = Mock<IRepository>()
                            .Setup(fun x -> <@ x.GetNode("1") @>).Returns(Task.FromResult(fixture.Create<Node>()))
                            .Setup(fun x -> <@ x.GetNode("2") @>).Returns(Task.FromResult(null))
                            .Create()

        getController(repository)

        //Act:
        let result = controller.Get("1","2").Result

        //Assert:
        result |> should be instanceOfType<BadRequestResult>

    [<Test>]
    member this.``Path controller - Get method should return the shortest path``() =
        //Arrange:
        let mutable repository = Mock<IRepository>()
                                    .Setup(fun x -> <@ x.ToString() @>).Returns("test repository")

        [|
            new Node( id = "1", label = "node1", adjacentNodes = new List<string>([ "2"; "3" ]));
            new Node( id = "2", label = "node2", adjacentNodes = new List<string>([ "1"; "3" ]));
            new Node( id = "3", label = "node3", adjacentNodes = new List<string>([ "1"; "2"; "5" ]));
            new Node( id = "4", label = "node4", adjacentNodes = new List<string>([ "3" ]));
            new Node( id = "5", label = "node5", adjacentNodes = new List<string>([ "3" ]))
        |]
        |> Array.map (fun x -> repository <- setupNodeEntity(repository, x)) |> ignore
        
        getController(repository.Create())

        //Act:
        let result = controller.Get("1","5").Result

        //Assert:
        result |> should be instanceOfType<OkNegotiatedContentResult<List<string>>>
        (result :?> OkNegotiatedContentResult<List<string>>).Content |> should equal (new List<string>([ "5";"3";"1" ]))

