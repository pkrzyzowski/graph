﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Graph.DTO;
using Graph.Api.Headers;
using Graph.Api.Client;

namespace Graph.DataLoader
{
    class Program
    {
        static XmlSerializer xmlSerializer = new XmlSerializer(typeof(Node));

        private static IEnumerable<Node> ReadNodesFromDataDirectory(string dataDirectory)
        {
            foreach (string file in Directory.GetFiles(dataDirectory))
            {
                yield return DeserializeNodeFromFile(file);
            }
        }

        private static Node DeserializeNodeFromFile(string path)
        {
            using (StreamReader fileStream = new StreamReader(path))
            {
                return (Node)xmlSerializer.Deserialize(fileStream);
            }
        }
        
        private static bool CheckIfNodeExist(GraphApiClient client, string nodeId)
        {
            return client.Get(client.BuildUrl("/api/get/" + nodeId)).Result != null;
        }

        private static void AddReplaceNodes(GraphApiClient client, string dataDirectory)
        {
            IEnumerable<Node> nodes = ReadNodesFromDataDirectory(dataDirectory);
            foreach (Node node in nodes)
            {
                Console.WriteLine("Uploading node {0}...", node.id);
                HttpStatusCode result;
                if (CheckIfNodeExist(client, node.id))
                {
                    result = client.PutNode(node).Result;
                }
                else
                {
                    result = client.PostNode(node).Result;
                }
                Console.WriteLine("Status: {0}", result);
            }
        }

        private static bool NodeExistsInDirectory(string dataDirectory, string nodeId)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Node));

            foreach(string file in Directory.EnumerateFiles(dataDirectory, "*.xml"))
            {
                Node node = DeserializeNodeFromFile(file);
                if (node.id == nodeId) return true;
            }
            return false;
        }

        private static void RemoveNodesFromDatabase(GraphApiClient client, string dataDirectory, int batchSize, string url = null)
        {
            HttpResponseMessage response = string.IsNullOrWhiteSpace(url) ? client.GetNodes(1, batchSize).Result : client.Get(url).Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (!response.Headers.Contains(PaginationHeader.PaginationHeaderName))
                {
                    Console.WriteLine("ERROR: Couldn't get pagination header from response!");
                    return;
                }
                PaginationHeader paginationHeader = PaginationHeader.Parse(response.Headers.Single(p => p.Key == PaginationHeader.PaginationHeaderName).Value.First());

                IEnumerable<Node> nodes = JsonConvert.DeserializeObject<IEnumerable<Node>>(response.Content.ReadAsStringAsync().Result);
                foreach (Node node in nodes)
                {
                    if (!NodeExistsInDirectory(dataDirectory, node.id))
                    {
                        Console.WriteLine("Removing node: {0}", node.id);
                        if(client.DeleteNode(node.id).Result.StatusCode == HttpStatusCode.NoContent)
                        {
                            Console.WriteLine("Node removed.");
                        } else
                        {
                            Console.WriteLine("ERROR: Couldn't remove node!");
                        }
                    }
                }

                if (paginationHeader.NextPage != null)
                {
                    RemoveNodesFromDatabase(client, dataDirectory, batchSize, paginationHeader.NextPage);
                }
            }
            else if (response.StatusCode == HttpStatusCode.NoContent) return;
        }

        static void Main(string[] args)
        {
            string dataDirectory = ConfigurationManager.AppSettings["DataDirectory"];
            string apiBaseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"];
            int readingBatchSize = int.Parse(ConfigurationManager.AppSettings["ReadingBatchSize"]);

            using (GraphApiClient client = new GraphApiClient(apiBaseUrl))
            {
                RemoveNodesFromDatabase(client, dataDirectory, readingBatchSize);
                AddReplaceNodes(client, dataDirectory);
            }
        }
    }
}
