﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Graph.DTO
{
    [XmlRoot("node")]
    public class Node
    {
        [XmlElement("id")]
        public string id { get; set; }

        [XmlElement("label")]
        public string label { get; set; }

        [XmlArray("adjacentNodes")]
        [XmlArrayItem(ElementName ="id")]
        public List<string> adjacentNodes { get; set; }
    }
}
