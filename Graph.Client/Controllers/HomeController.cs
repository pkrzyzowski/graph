﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Graph.Api.Client;
using System.Configuration;

namespace Graph.Client.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.apiBaseUrl = ConfigurationManager.AppSettings["Graph.Api.Url"];
            return View();
        }
    }
}