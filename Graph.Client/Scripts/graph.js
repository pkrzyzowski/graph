﻿var apiBaseUrl;
var graph;
var selectedNodes = new Array();
var calculated = false;

var nodeColor = '#ff0000';
var selectedNodeColor = '#0000ff';
var shortestPathNodeColor = '#00ff00';
var edgeColor = '#000000';

function checkIfEdgeExist(source, destination) {
    var edges = graph.graph.edges();
    for (var index = 0, edgesCount = edges.length; index < edgesCount; index++) {
        var currentEdge = edges[index];

        return
        (currentEdge.source == source && currentEdge.destination == destination)
        || (currentEdge.source == destination && currentEdge.destination == source);
    }
}

function checkIfNodeExist(nodeId) {
    return graph.graph.nodes(nodeId) != undefined;
}

function checkIfNodeIsSelected(nodeId) {
    for (var i = 0; i < selectedNodes.length; i++) {
        if (selectedNodes[i] == nodeId) {
            return true;
        }
    }
    return false;
}

function changeNodeColor(nodeId, color) {
    $.each(graph.graph.nodes(), function (i, n) {
        if (nodeId == n.id) {
            n.color = color;
        }
    });
}

function addEdge(source, destination) {
    if (!checkIfEdgeExist(source, destination)) {

        if (!checkIfNodeExist(source) || !checkIfNodeExist(destination)) return;

        graph.graph.addEdge({
            id: Math.random(),
            source: source,
            target: destination,
            size: 3,
            color: edgeColor
        });
    }
}

function calculateShortestPath() {
    if (selectedNodes.length == 2) {
        $.ajax({
            url: apiBaseUrl + '/api/path/' + selectedNodes[0] + '/' + selectedNodes[1],
            dataType: 'json',
            success: function (response) {
                $.each(response, function (i) {
                    changeNodeColor(response[i], shortestPathNodeColor);
                });
                calculated = true;
                graph.refresh();
            }
        });
    }
}

function setNodeAsMarked(clickedNode) {
    if (checkIfNodeIsSelected(clickedNode.id)) {
        return;
    }

    if (calculated == true) {
        for (var i = 0; i < graph.graph.nodes().length; i++){
            changeNodeColor(graph.graph.nodes()[i].id, nodeColor);
        }
        selectedNodes = [];
        calculated = false;
    }

    if (selectedNodes.length == 2) {
        changeNodeColor(selectedNodes[0], nodeColor)
        selectedNodes.shift();
    }

    changeNodeColor(clickedNode.id, selectedNodeColor)
    selectedNodes.push(clickedNode.id);
    graph.refresh();
}

function drawGraph() {
    $.ajax({
        url: apiBaseUrl + '/api/node',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (index, node) {
                graph.graph.addNode({
                    id: node.id,
                    label: node.label,
                    x: Math.random(),
                    y: Math.random(),
                    size: 8,
                    color: nodeColor,
                    borderColor: '#000'
                });
            });

            $.each(response, function (index, node) {
                $.each(node.adjacentNodes, function (i, adjacent) {
                    addEdge(node.id, adjacent);
                });
            });

            graph.refresh();

            graph.bind('clickNode', function (e) {
                setNodeAsMarked(e.data.node);
            });
        }
    });
}

$(document).ready(function () {
    apiBaseUrl = $('#baseUrl').attr('data-content');

    graph = new sigma('container');
    drawGraph();

    $('#calculate').click(function () {
        calculateShortestPath();
    });

});