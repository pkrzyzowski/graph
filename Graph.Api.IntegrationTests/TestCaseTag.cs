﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph.Api.IntegrationTests
{
    public class TestCaseTag
    {
        public static string Uri = "Uri";
        public static string Method = "Method";
        public static string RequestBody = "RequestBody";
        public static string ExpectedResponseType = "ExpectedResponseType";
        public static string ExpectedResponseBody = "ExpectedResponseBody";
        public static string ExpectedStatusCode = "ExpectedStatusCode";
        public static string ExpectedPaginationHeader = "ExpectedPaginationHeader";
    }
}
