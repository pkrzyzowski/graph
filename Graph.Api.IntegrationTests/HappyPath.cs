﻿using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Net.Http;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Web.Http.Results;
using System.Net;

using Mongo2Go;
using MongoDB.Driver;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Graph.DTO;
using Graph.Api.Headers;
using System.Linq;

namespace Graph.Api.IntegrationTests
{
    [TestClass]
    public class HappyPath : IDisposable
    {
        private MongoDbRunner testMongoDbServer;
        private TestServer testApiServer;
        private static string dataDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Path");
        private bool disposed = false;
        private TestContext testContext;
        private string mongoDbDatabaseName;

        public TestContext TestContext
        {
            get { return testContext; }
            set { testContext = value; }
        }

        private void RemoveDatabase(string connectionString, string databaseName)
        {
            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            this.testMongoDbServer = MongoDbRunner.StartForDebugging(dataDirectory);
            Configuration config = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
            config.AppSettings.Settings.Remove("MongoDbConnectionString");
            config.AppSettings.Settings.Add("MongoDbConnectionString", testMongoDbServer.ConnectionString);
            mongoDbDatabaseName = config.AppSettings.Settings["MongoDbDatabaseName"].Value;

            this.testApiServer = TestServer.Create<Startup>();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Dispose();
        }

        private object ReadTestCaseSetting(string settingName)
        {
            if (this.testContext.DataRow.Table.Columns.Contains(settingName))
                return this.testContext.DataRow[settingName];
            else return null;
        }

        private object DeserializeBody(Type objectType, string body)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject(body, objectType);
        }

        private HttpResponseMessage SendRequest(string url, string method, string requestBody = null)
        {
            RequestBuilder request = this.testApiServer.CreateRequest(url);
            if(!string.IsNullOrWhiteSpace(requestBody))
            {
                Type nodeType = typeof(Node);
                request.And(r => r.Content = new ObjectContent(nodeType, DeserializeBody(nodeType, requestBody), new JsonMediaTypeFormatter()));
            }

            return request.SendAsync(method).Result;
        }

        private bool CompareNodes(Node expected, Node actual)
        {
            return expected.id == actual.id
                && expected.label == actual.label
                && expected.adjacentNodes.SequenceEqual(actual.adjacentNodes);
        }

        private bool IsResponseBodyValid(string expectedResponseType, string expectedValue, string actualValue)
        {
            switch (expectedResponseType)
            {
                case "Node":
                    {
                        Type nodeType = typeof(Node);
                        Node expectedNode = (Node)DeserializeBody(nodeType, expectedValue);
                        Node actualNode = (Node)DeserializeBody(nodeType, actualValue);

                        return CompareNodes(expectedNode, actualNode);
                    }
                case "Node[]":
                    {
                        Type nodeType = typeof(Node[]);
                        Node[] expectedNodes = (Node[])DeserializeBody(nodeType, expectedValue);
                        Node[] actualNodes = (Node[])DeserializeBody(nodeType, actualValue);

                        bool areNodesEqual = true;
                        //I assumed here that person who was preparing the test scenario, prepared expected array in correct order
                        for (int i = 0; i < expectedNodes.Length; i++)
                        {
                            areNodesEqual &= CompareNodes(expectedNodes[i], actualNodes[i]);
                        }
                        return areNodesEqual;
                    }
                case "List":
                    {
                        Type listType = typeof(List<string>);
                        List<string> expectedList = (List<string>)DeserializeBody(listType, expectedValue);
                        List<string> actualList = (List<string>)DeserializeBody(listType, actualValue);

                        return expectedList.SequenceEqual(actualList);
                    }
                default:
                    return expectedResponseType == actualValue;
            }
        }

        private bool ComparePaginationHeaders(PaginationHeader expected, PaginationHeader actual)
        {
            return expected.CurrentPage == actual.CurrentPage
                && expected.NextPage == actual.NextPage
                && expected.PagesCount == actual.PagesCount
                && expected.PagesCount == actual.PageSize
                && expected.PreviousPage == actual.PreviousPage;
        }

        [TestMethod]
        [TestCategory("IntegrationTests")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "HappyPath.xml", "Request", DataAccessMethod.Sequential)]
        public void IntegrationTests_HappyPath()
        {
            if(testContext.DataRow.Table.Rows.IndexOf(testContext.DataRow)==0)
            {
                RemoveDatabase(this.testMongoDbServer.ConnectionString, this.mongoDbDatabaseName);
            }
            //Arrange:
            string url = ReadTestCaseSetting(TestCaseTag.Uri)?.ToString();
            string method = ReadTestCaseSetting(TestCaseTag.Method)?.ToString();
            string requestBody = ReadTestCaseSetting(TestCaseTag.RequestBody)?.ToString();
            string expectedResponseType = ReadTestCaseSetting(TestCaseTag.ExpectedResponseType)?.ToString();
            string expectedResponseBody = ReadTestCaseSetting(TestCaseTag.ExpectedResponseBody)?.ToString();
            int expectedStatusCode = int.Parse(ReadTestCaseSetting(TestCaseTag.ExpectedStatusCode).ToString());
            string expectedPaginationHeader = ReadTestCaseSetting(TestCaseTag.ExpectedPaginationHeader)?.ToString();
            bool validateResponseBody = !string.IsNullOrWhiteSpace(expectedResponseBody);

            RequestBuilder request = this.testApiServer.CreateRequest("/api/node");
            request.And(r => r.Content = new ObjectContent(typeof(Node), new Node() { id = "1", adjacentNodes = new List<string>() { "2" }, label = "test" }, new JsonMediaTypeFormatter()));

            //Act:
            var response = SendRequest(url, method, requestBody);

            //Assert:
            Assert.IsNotNull(response);
            Assert.AreEqual(expectedStatusCode, (int)response.StatusCode);

            if(validateResponseBody)
            {
                Assert.IsTrue(IsResponseBodyValid(expectedResponseType, expectedResponseBody, response.Content.ReadAsStringAsync().Result));
            }

            if (!string.IsNullOrWhiteSpace(expectedPaginationHeader))
            {
                Assert.IsTrue(response.Headers.Contains(PaginationHeader.PaginationHeaderName));
                ComparePaginationHeaders(PaginationHeader.Parse(expectedPaginationHeader), PaginationHeader.Parse(response.Headers.Single(p => p.Key == PaginationHeader.PaginationHeaderName).Value.First()));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if (disposing)
            {
                this.testMongoDbServer.Dispose();
                this.testApiServer.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
