﻿using System;
using System.Linq;
using System.Collections.Generic;
using MongoDB;
using MongoDB.Driver;
using System.Configuration;

using Graph.DTO;
using MongoDB.Bson;
using System.Threading.Tasks;

namespace Graph.Api.Repository
{
    public class MongoDbRepository : IRepository, IDisposable
    {
        private const string NodesCollectionName = "Nodes";
        private bool disposed = false;
        private MongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<Node> nodesCollection;

        private void Setup(string connectionString, string databaseName)
        {
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ConfigurationErrorsException("Missing MongoDbConnectionString key in AppSettings");

            this.client = new MongoClient(connectionString);
            this.database = this.client.GetDatabase(databaseName);

            CreateCollections();
        }
        
        public MongoDbRepository()
        {
            string connectionString = ConfigurationManager.AppSettings["MongoDbConnectionString"];
            string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];

            Setup(connectionString, databaseName);
        }

        private void CreateCollection(string collectionName)
        {

            this.nodesCollection = database.GetCollection<Node>(collectionName);
            if (this.nodesCollection == null) database.CreateCollection(collectionName);

            this.nodesCollection.Indexes.CreateOne(Builders<Node>.IndexKeys.Ascending(p=>p.id), new CreateIndexOptions() { Unique = true });
        }

        private void CreateCollections()
        {
            CreateCollection(NodesCollectionName);
        }

        public Task<IEnumerable<Node>> FindTheShortestPath(Node startingPoint, Node destination)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> NodeExists(string id)
        {
            var result = await this.nodesCollection.FindAsync(
                v => v.id == id
            );
            return result.Any();
        }

        public async Task<Node> GetNode(string id)
        {
            var result = await this.nodesCollection.FindAsync(
                v=>v.id == id
            );
            return result.SingleOrDefault();
        }

        public async Task<long> CountNodes()
        {
            long result = await this.nodesCollection.CountAsync(new BsonDocument());
            return result;
        }

        public async Task<IEnumerable<Node>> GetNodes(int? skip = null, int? take = null)
        {
            FindOptions<Node> findOptions = skip != null ? new FindOptions<Node> { Skip = skip, Limit = take } : new FindOptions<Node>();

            var result = await this.nodesCollection.FindAsync<Node>(
                new BsonDocument(),
                findOptions
            );

            return result.ToEnumerable();
        }

        public async Task<Node> InsertNode(Node node)
        {
            await this.nodesCollection.InsertOneAsync(node);
            return node;
        }

        public async Task<long> RemoveNode(string id)
        {
            DeleteResult result = await this.nodesCollection.DeleteOneAsync(
                v=>v.id == id
            );
            
            if (result.IsAcknowledged)
                return result.DeletedCount;
            else
                return 0;
        }

        public async Task<Node> UpdateNode(Node node)
        {
            ReplaceOneResult result = await this.nodesCollection.ReplaceOneAsync(
                Builders<Node>.Filter.Eq("id", node.id),
                node
            );
            if (result.IsAcknowledged && result.ModifiedCount > 0)
            {
                return node;
            }
            return null;
        }

        public async Task<bool> AddAdjacent(string nodeId, string adjacentNodeId)
        {
            UpdateResult result = await this.nodesCollection.UpdateOneAsync(
                Builders<Node>.Filter.Eq("id", nodeId),
                Builders<Node>.Update.AddToSet("adjacentNodes", adjacentNodeId)
            );

            return (result.IsAcknowledged && result.ModifiedCount > 0);
        }

        public async Task<IEnumerable<string>> FindAdjacents(string nodeId)
        {
            return await this.nodesCollection.Find(x => x.adjacentNodes.Contains(nodeId)).Project(x => x.id).ToListAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if (disposing)
            {
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
