﻿using Graph.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Graph.Api.Repository
{
    public interface IRepository
    {
        Task<Node> InsertNode(Node node);
        Task<long> RemoveNode(string id);
        Task<Node> UpdateNode(Node node);
        Task<Node> GetNode(string id);
        Task<bool> NodeExists(string id);
        Task<bool> AddAdjacent(string nodeId, string adjacentNodeId);
        Task<IEnumerable<string>> FindAdjacents(string nodeId);
        Task<long> CountNodes();
        Task<IEnumerable<Node>> GetNodes(int? skip = null, int? take = null);
    }
}
