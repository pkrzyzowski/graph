﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph.Api.Headers
{
    public class PaginationHeader
    {
        public static string PaginationHeaderName = "Pagination";

        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int PagesCount { get; set; }
        public string PreviousPage { get; set; }
        public string NextPage { get; set; }

        public static PaginationHeader Parse(string serializedPaginationHeader)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<PaginationHeader>(serializedPaginationHeader);
        }
    }
}
