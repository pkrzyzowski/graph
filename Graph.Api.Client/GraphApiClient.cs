﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Graph.Api.Headers;
using Graph.DTO;
using System.Net.Http.Headers;

namespace Graph.Api.Client
{
    public class GraphApiClient : IDisposable
    {
        private Uri baseUri;
        private HttpClient client;
        private bool disposed = false;

        public GraphApiClient(string baseUrl)
        {
            this.baseUri = new Uri(baseUrl);
            this.client = new HttpClient
            {
                BaseAddress = this.baseUri
            };
            this.client.DefaultRequestHeaders.Accept.Clear();
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public string BuildUrl(string url)
        {
            return new Uri(baseUri, url).ToString();
        }

        public async Task<HttpStatusCode> PostNode(Node node)
        {
            var response = await client.PostAsJsonAsync("api/node", node);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> PutNode(Node node)
        {
            var response = await client.PutAsJsonAsync("api/node", node);
            return response.StatusCode;
        }

        public async Task<HttpResponseMessage> GetNodes(int currentPage, int pageSize)
        {
            return await client.GetAsync(string.Format("/api/nodes/{0}/{1}", currentPage, pageSize));
        }

        public async Task<HttpResponseMessage> Get(string url)
        {
            return await client.GetAsync(url);
        }

        public async Task<HttpResponseMessage> DeleteNode(string nodeId)
        {
            return await client.DeleteAsync(string.Format("/api/node/{0}", nodeId));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if (disposing)
            {
                client.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
