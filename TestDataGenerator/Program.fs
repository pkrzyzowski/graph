﻿//I used F# here, as it's just a support project to generate test data.
open System
open System.IO
open System.Xml
open System.Xml.Linq

type node = { 
    id:string; 
    label:string; 
    adjacentNodes:List<string>; 
}

let testDataDir  = "E:\xmls"
let numberOfNodes = 15
let maxNumberOfAdjacents = 5
let random = new Random()

let generateName() =
    Guid.NewGuid().ToString().Substring(0,4)

let rec getRandomId(currentId:int) = 
    let randomId = random.Next(1, numberOfNodes+1)

    match randomId with
    | a when a = currentId -> getRandomId(currentId)
    | _ -> randomId

let generateAdjacents(id) = 
    let exclusionList = []
    let adjacents = [for i in 1 .. random.Next(1, maxNumberOfAdjacents) -> getRandomId(id).ToString()]
                    |> List.distinct

    adjacents

let generateNode (id:int) = 
    { id = id.ToString(); label = generateName(); adjacentNodes = generateAdjacents(id)}

let XElement(name, content) =
    let element = new XElement(XName.Get name)
    element.Value <- content
    element

let XElementWithChilds(name, childs:XElement[]) =
    new XElement(XName.Get name, childs)

let generateXml(node:node) =
    let x = new XDocument()

    let adjacents = node.adjacentNodes
                    |> List.map (fun x -> XElement("id", x))
                    |> List.toArray

    let root = XElementWithChilds("node", [| XElement("id", node.id) ; XElement("label", node.label) ; XElementWithChilds("adjacentNodes", adjacents) |])
    x.Add(root)
    x.Save(Path.Combine(testDataDir, node.label + ".xml"))
    

[<EntryPoint>]
let main argv = 

    Directory.CreateDirectory(testDataDir) |> ignore

    let nodes = [1..numberOfNodes]
                |> List.map generateNode
                |> List.map generateXml

//    printfn "%A" nodes
    0 // return an integer exit code
